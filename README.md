
Copyright <2022> Philipe Lizotte
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**

## Description du projet

**
Ce logiciel permet à l'utilisateur de créer différentes formes à l'aide d'un menu interactif. 
L'utilisateur choisit les coordonnées auxquelles il veut créer la forme ainsi que les couleurs.
Il peut choisir entre créer une forme avec une seule couleur qui sera la couleur de contour ou il
peut choisir une forme avec deux couleurs. La deuxième couleur servira pour l'intérieur de la forme.
voci les formes que vous pouvez créer.

 - Carre
 - Rectangle
 - Cercle
 - Elipse
 - Ligne
 - Triangle

**

## Compilation

**
Ouvrer cmd , naviguer ensuite jusqu'au dossier /src du Projet.
Tapper ensuite la commande suivant : javac Main.java.

**

## Exécutions

**
une fois le Main.java compiler vous pouvez effectuer la commande suivante: java Main.java