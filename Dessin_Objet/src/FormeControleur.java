
/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * controleur du programe cet class hérite de l'interface
 * observeur, et gere les vues
 */
public class FormeControleur implements  Observer{
    /**
     * la fenetre du programe
     */
    private VueFenetre vueFenetre;
    /**
     * la grille de dessin du programe
     */
    private VueGrille vueGrille;
    /**
     * le menu du programe
     */
    private VueMenu vueMenu;


    /**
     * getter de VueMenu permet de retourner le menu du programe
     * @return le menu du programe
     */
    public VueMenu getVueMenu(){return vueMenu;}

    /**
     * constructeur de FromeControleur
     * initialise les vues et ajoute un observeur au menu
     */
    public FormeControleur() {

         vueGrille =new VueGrille();
         vueMenu = new VueMenu();
         vueMenu.ajouterObserver(this);
         vueFenetre = new VueFenetre(vueMenu,vueGrille);
    }

    /**
     * Méthode redéfini de l'interface Observeur
     * elle sert à actualiser la grille de dessin lorsque
     * avec la list de forme dans VueMenu lorsque
     * la méthode est appeller
     * @param aobservable l'observable à actualiser
     */
    @Override
    public void ajouterForme(Observable aobservable) {
        if(aobservable instanceof VueMenu) {
            VueMenu vueMenu = (VueMenu) aobservable;
            vueGrille.setListForm(vueMenu.getListeForme());
        }
    }
public void test(){
    Couleur bleu = new Couleur(10,10,255);
    Couleur rouge = new Couleur(255,10,10);
    Couleur vert = new Couleur(10,255,10);
    FormeControleur aformeControleur = new FormeControleur();
    aformeControleur.getVueMenu().getListeForme().add(new Rectangle(bleu,bleu,10,10,100,250,true));
    aformeControleur.getVueMenu().getListeForme().add(new Carre(vert,rouge,300,300,50,true));
    aformeControleur.getVueMenu().getListeForme().add(new Cercle(rouge,bleu,50,260,50,true));
    aformeControleur.getVueMenu().getListeForme().add(new Elipse(rouge,bleu,400,400,100,50,true));
    aformeControleur.getVueMenu().getListeForme().add(new Triangle(rouge,bleu,120,150,160,300,310,290,true));
    aformeControleur.getVueMenu().getListeForme().add(new Ligne(rouge,400,100,100,400));
    aformeControleur.getVueMenu().getListeForme().add(new Ligne(vert,600,100,50,200));

    aformeControleur.getVueMenu().informerObserver();
}
}
