/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * interface abstrait de Observeur
 */
public interface Observer{
    /**
     * méthode abstraite qui est exécuter lorsqu'un observeur est
     * informer par un observable ceci modifi l'observable
     * @param aobservable l'observable à actualiser
     */
    void ajouterForme(Observable aobservable);
}
