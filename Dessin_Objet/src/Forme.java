

/**
 * @author  Philipe Lizotte
 * @version %I%, %G%
 * Classe du modele elle est la classe parent de toutes
 * les autres classe du modele
 */
public class Forme {
    /**
     * la couleur de la forme (la couleur du contour pour les formes
     * avec une une couleur centrale)
     */
    private Couleur couleur;

    /**
     * constructeur de la classe Forme
     * @param aCouleur la couleur que la forme vas avoir
     */
    public Forme(Couleur aCouleur){
        setCouleur(aCouleur);
    }

    /**
     * getter de couleur
     * @return la couleur de la forme
     */
    public Couleur getCouleur() {
        return couleur;
    }

    /**
     * setter de couleur
     * @param couleur la couleur que sera 'setter' pour la forme
     */
    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }
}
