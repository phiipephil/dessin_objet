
import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.FlowLayout;

/**
 * @author Philipe Lizotte
 * @author Philipe Lizotte
 * vue de la fenetre du programe, elle
 * contient VueMenu et VueGrille
 */
public class VueFenetre {
    /**
     * constructeur de VueFenetre
     * @param vueMenu le menu ajouter à la fenetre
     * @param vueGrille la grille de dessin ajouter à la fenetre
     */
    public VueFenetre(VueMenu vueMenu, VueGrille vueGrille) {
        JFrame lFenetre = new JFrame("ProjetDessin");
        lFenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        lFenetre.setLayout(new FlowLayout());
        lFenetre.setVisible(true);

        lFenetre.setLayout(new GridLayout(1,1));
        vueMenu.getJpanel().setLayout(new FlowLayout());


        lFenetre.add(vueMenu.getJpanel());
        lFenetre.add(vueMenu.getJpanel2());
        lFenetre.add(vueMenu.getJpanel3());
        lFenetre.add(vueGrille);
        lFenetre.pack();

}
    }
