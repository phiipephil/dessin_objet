import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * vue de la grille de dessin du programe elle hérite
 * de Jpanel
 */
class VueGrille extends JPanel  {
    /**
     * la list de forme qui est dessiner dans la vue
     */
    private LinkedList<Forme> listForm;



    /**
     * constructeur de VueGrille
     * initialise la list de formes
     */
    public VueGrille() {
        listForm = new LinkedList<>();
    }

    /**
     * redéfinition de la méthode paintComponent
     * cette méthode peint les formes de la liste de formes
     * dans l'élément graphique envoyer
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    @Override
    public void paintComponent(Graphics graphique) {
        Graphics2D g2d = (Graphics2D) graphique;
        g2d.setStroke(new BasicStroke(5));
        dessinerForme(g2d,getListForm());
    }

    /**
     * getter de la grosseur de la grille
     * @return la grosseur de la grille
     */
    public Dimension getPreferredSize() {
        return new Dimension(500,500);
    }


    /**
     * permet d'appeller les fonctions permetant de dessiner les formes
     * pour chaque formes de la list envoyer
     * @param graphique l'élément grafique dans lequel on vas dessiner
     * @param  alistForm la liste de formes qui sera dessiner
     */
    public void dessinerForme(Graphics graphique, List<Forme> alistForm){
        for(Forme forme: alistForm){
           if(forme instanceof Rectangle) {
               dessinerRectangle(forme,graphique);
           }else if(forme instanceof Carre){
               dessinerCarre(forme,graphique);
           }else if(forme instanceof Cercle){
               dessinerCercle(forme,graphique);
           }else if(forme instanceof Elipse){
               dessinerElipse(forme,graphique);
           }else if(forme instanceof Triangle){
               dessinerTriangle(forme,graphique);
           }else if(forme instanceof Ligne){
               dessinerLigne(forme,graphique);
           }
        }
    }

    /**
     * permet de dessiner un Triangle
     * @param ltriangle le triangle à dessiner
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    public void dessinerTriangle(Forme ltriangle, Graphics graphique) {
        Triangle triangle = (Triangle) ltriangle;
        int[] pointX ={triangle.getPointX1(),triangle.getPointX2(),
                triangle.getPointx3()};
        int[] pointY = {triangle.getPointY1(),triangle.getPointY2(),
                triangle.getPointY3()};

        graphique.setColor(triangle.getCouleur());
        graphique.drawPolygon(pointX,pointY,3);
        if(triangle.isRempli()) {
            graphique.setColor(triangle.getCouleurCentral());
            graphique.fillPolygon(pointX, pointY, 3);
        }
    }

    /**
     * permet de dessiner un rectangle
     * @param lrectangle le rectangle à dessiner
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    public void dessinerRectangle(Forme lrectangle,Graphics graphique){
        Rectangle rectangle = (Rectangle) lrectangle;
        graphique.setColor(rectangle.getCouleur());
        graphique.drawRect(
                rectangle.getXGauche(),
                rectangle.getyHaut(),
                rectangle.getLargeur(),
                rectangle.getHauteur()
        );
        graphique.setColor(rectangle.getCouleurCentral());
        graphique.fillRect(rectangle.getXGauche(),
                rectangle.getyHaut(),
                rectangle.getLargeur(),
                rectangle.getHauteur());

    }

    /**
     * permet de dessiner une ligne
     * @param lligne la ligne à dessiner
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    public void dessinerLigne(Forme lligne, Graphics graphique) {
        Ligne ligne = (Ligne) lligne;
        graphique.setColor(ligne.getCouleur());
        graphique.drawLine(ligne.getPointX1(),ligne.getPointY1(),
                ligne.getPointX2(),ligne.getPointY2());
    }

    /**
     * permet de dessiner un Elipse
     * @param lelipse l'elipse à dessiner
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    public void dessinerElipse(Forme lelipse, Graphics graphique) {
        Elipse elipse = (Elipse) lelipse;
        graphique.setColor(elipse.getCouleur());
        graphique.drawOval(elipse.getCentreX()/2,
                elipse.getCentreY()/2,
                elipse.getLargeur(),
                elipse.getHauteur());
        graphique.setColor(elipse.getCouleurCentral());
        graphique.fillOval(elipse.getCentreX()/2,
                elipse.getCentreY()/2,
                elipse.getLargeur(),
                elipse.getHauteur());
    }

    /**
     * permet de dessiner un cercle
     * @param lcercle le cercle à dessiner
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    public void dessinerCercle(Forme lcercle, Graphics graphique) {
        Cercle cercle = (Cercle) lcercle;
        graphique.setColor(cercle.getCouleur());
        graphique.drawOval(cercle.getCentreX()/2,
                cercle.getCentreY()/2,
                cercle.getRayon(),
                cercle.getRayon());
        graphique.setColor(cercle.getCouleurCentral());
        graphique.fillOval(cercle.getCentreX()/2,
                cercle.getCentreY()/2,
                cercle.getRayon(),
                cercle.getRayon());

    }

    /**
     * permet de dessiner un carre
     * @param lcarre le carre à dessiner
     * @param graphique l'élément grafique dans lequel on vas dessiner
     */
    public void dessinerCarre(Forme lcarre, Graphics graphique) {
        Carre carre = (Carre) lcarre;
        graphique.setColor(carre.getCouleur());
        graphique.drawRect(carre.getxGauche(),
                carre.getyHaut(),
                carre.getLargeur(),
                carre.getLargeur());
        if(((Carre) lcarre).isRempli()) {
            graphique.setColor(carre.getCouleurCentral());
            graphique.fillRect(carre.getxGauche(),
                    carre.getyHaut(),
                    carre.getLargeur(),
                    carre.getLargeur());
        }
    }

    /**
     * setter de listForm
     * @param llistForm la list de forme qui sera dessiner
     */
    public void setListForm(LinkedList<Forme> llistForm){
        listForm = llistForm;
    }

    /**
     * getter de listForm
     * @return la list de forme
     */
    public LinkedList<Forme> getListForm() {
        return listForm;
    }

}
