

/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * class du modèle que represente une elipse elle hérite
 * de la classe FormeCouleurCentral
 */
public class Elipse extends FormeCouleurCentral {
    /**
     * la coordonner en X du centre de l'elipse
     */
    private int centreX;
    /**
     * la coordonner en Y du centre de l'elipse
     */
    private int centreY;
    /**
     * la hauteur de l'elipse
     */
    private int hauteur;
    /**
     * la largeur de l'elipse
     */
    private int largeur;

    /**
     * constructeur de la class elipse
     * utilise le constructur de sa classe parent
     * @param aCouleur la couleur du contour de l'elipse
     * @param aCouleurCentral la couleur du centre de l'elipse
     * @param centreX la coordonner en X du centre de l'elipse
     * @param centreY  la coordonner en Y du centre de l'elipse
     * @param hauteur la hauteur de l'elipse
     * @param largeur la largeur de l'elipse
     * @param arempli un boolean affin de savoir si l'elipse serra rempli
     */
    public Elipse(Couleur aCouleur, Couleur aCouleurCentral,
                  int centreX, int centreY,
                  int hauteur, int largeur,boolean arempli) {
        super(aCouleur,aCouleurCentral,arempli);
        setCentreX(centreX);
        setCentreY(centreY);
        setHauteur(hauteur);
        setLargeur(largeur);
    }

    /**
     * getter de centreX
     * @return la coordonner en X du centre de l'elipse
     */
    public int getCentreX() {
        return centreX;
    }

    /**
     * setter de centreX
     * @param centreX la coordonner en X qui sera 'setter' pour l'elipse
     */
    public void setCentreX(int centreX) {
        this.centreX = centreX;
    }

    /**
     * getter de centreY
     * @return la coordonner en Y du centre de l'elipse
     */
    public int getCentreY() {
        return centreY;
    }

    /**
     * setter de centreY
     * @param centreY la coordonner en Y qui sera 'setter' pour l'elipse
     */
    public void setCentreY(int centreY) {
        this.centreY = centreY;
    }

    /**
     * getter de hauteur
     * @return la hauteur de l'elipse
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * setter de hauteur
     * @param hauteur la hauteur que sera 'setter' pour l'elipse
     */
    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    /**
     * getter de largeur
     * @return la largeur de l'elipse
     */
    public int getLargeur() {
        return largeur;
    }
    /**
     * setter de largeur
     * @param largeur la largeur que sera 'setter' pour l'elipse
     */
    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }
}