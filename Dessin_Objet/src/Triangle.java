

/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * class du modèle représantant un triangle
 * hérite de FormCouleurCentral
 */
public class Triangle extends FormeCouleurCentral{
    /**
     * la coordonner en X du premier point du triangle
     */
    private int pointX1;
    /**
     * la coordonner en X du deuxieme point du triangle
     */
    private int pointX2;
    /**
     * la coordonner en X du troisieme point du triangle
     */
    private int pointx3;
    /**
     * la coordonner en Y du premier point du triangle
     */
    private int pointY1;
    /**
     * la coordonner en Y du deuxieme point du triangle
     */
    private int pointY2;
    /**
     * la coordonner en Y du troisieme point du triangle
     */
    private int pointY3;

    /***
     * constructeur de la classe triangle utilise
     * le constructeur de sa class parent
     * @param aCouleur la couleur du contour du triangle
     * @param aCouleurCentral la couleur du centre du trangle
     * @param pointX1 la coordonner en X du premier point du trangle
     * @param pointX2 la coordonner en X du deuxieme point du triangle
     * @param pointx3 la coordonner en X du troisieme point du triangle
     * @param pointY1 la coordonner en Y du premier point du trangle
     * @param pointY2 la coordonner en Y du deuxieme point du triangle
     * @param pointY3 la coordonner en Y du troisieme point du triangle
     * @param arempli un boolean affin de savoir si le carre serra rempli
     */
    public Triangle(Couleur aCouleur, Couleur aCouleurCentral,
                    int pointX1, int pointX2,
                    int pointx3, int pointY1,
                    int pointY2, int pointY3,
                    boolean arempli) {

        super(aCouleur,aCouleurCentral,arempli);
        setPointX1(pointX1);
        setPointX2(pointX2);
        setPointx3(pointx3);
        setPointY1(pointY1);
        setPointY2(pointY2);
        setPointY3(pointY3);
    }

    /**
     * getter de pointX1
     * @return la coordonner en X du premier point du triangle
     */
    public int getPointX1() {
        return pointX1;
    }

    /**
     * setter de pointX1
     * @param pointX1 la coordonner en X du premier point du triangle à 'setter'
     */
    public void setPointX1(int pointX1) {
        this.pointX1 = pointX1;
    }
    /**
     * getter de pointX2
     * @return la coordonner en X du deuxieme point du triangle
     */
    public int getPointX2() {
        return pointX2;
    }

    /**
     * setter de pointX2
     * @param pointX2 la coordonner en X du deuxieme point du triangle à 'setter'
     */
    public void setPointX2(int pointX2) {
        this.pointX2 = pointX2;
    }
    /**
     * getter de pointX3
     * @return la coordonner en X du troisieme point du triangle
     */
    public int getPointx3() {
        return pointx3;
    }

    /**
     * setter de pointX3
     * @param pointx3 la coordonner en X du toisieme point du triangle à 'setter'
     */
    public void setPointx3(int pointx3) {
        this.pointx3 = pointx3;
    }
    /**
     * getter de pointY1
     * @return la coordonner en Y du premier point du triangle
     */
    public int getPointY1() {
        return pointY1;
    }

    /**
     * setter de pointY1
     * @param pointY1 la coordonner en Y du premier point du triangle à 'setter'
     */
    public void setPointY1(int pointY1) {
        this.pointY1 = pointY1;
    }
    /**
     * getter de pointY2
     * @return la coordonner en Y du deuxieme point du triangle
     */
    public int getPointY2() {
        return pointY2;
    }

    /**
     * setter de pointY2
     * @param pointY2 la coordonner en Y du deuxieme point du triangle à 'setter'
     */
    public void setPointY2(int pointY2) {
        this.pointY2 = pointY2;
    }
    /**
     * getter de pointY3
     * @return la coordonner en Y du troisieme point du triangle
     */
    public int getPointY3() {
        return pointY3;
    }

    /**
     * setter de point
     * @param pointY3
     */
    public void setPointY3(int pointY3) {
        this.pointY3 = pointY3;
    }

}