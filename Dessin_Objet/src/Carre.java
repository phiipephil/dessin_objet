

/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * class du modèle que represente un carre elle hérite
 * de la classe FormeCouleurCentral
 */
public class Carre extends FormeCouleurCentral{
    /**
     * les coordoners du coter gauche du carre en X
     */
    private int xGauche;
    /**
     * les coordoners du haut du carre en Y
     */
    private int yHaut;
    /**
     * la largeur du carre
     */
    private int largeur;

    /**
     * Constructeur de la classe carre
     * utiliser le constructeur de sa classe parent
     * pour les couleur du carre
     * @param aCouleur la couleur du contour du carre
     * @param aCouleurCentral la couleur centrale du carre
     * @param xGauche les coordoners du coter gauche du carre en X
     * @param yHaut les coordoners du haut du carre en Y
     * @param largeur la largeur du carre
     * @param arepmli un boolean affin de savoir si le carre serra rempli
     */
    public Carre(Couleur aCouleur, Couleur aCouleurCentral,
                 int xGauche, int yHaut, int largeur , boolean arepmli) {
        super(aCouleur,aCouleurCentral,arepmli);
        setxGauche(xGauche);
        setyHaut(yHaut);
        setLargeur(largeur);
    }

    /**
     * getter de xGauche
     * @return le xGauche du carre
     */
    public int getxGauche() {
        return xGauche;
    }

    /**
     * setter de xGauche
     * @param xGauche les coordonner en x qui sera 'setter'
     */
    public void setxGauche(int xGauche) {
        this.xGauche = xGauche;
    }

    /**
     * getter de yHaut
     * @return le yHaut du carre
     */
    public int getyHaut() {
        return yHaut;
    }

    /**
     * setter de yHaut
     * @param yHaut les coordonner en Y qui
     * seront 'setter' pour le carre
     */
    public void setyHaut(int yHaut) {
        this.yHaut = yHaut;
    }

    /**
     * getter de largeur
     * @return la largeur du carre
     */
    public int getLargeur() {
        return largeur;
    }

    /**
     * setter de largeur
     * @param largeur la largeur qui sera 'setter' pour le carre
     */
    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }
    
}