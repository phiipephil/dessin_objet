
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.colorchooser.AbstractColorChooserPanel;

import java.util.LinkedList;
/**
 * @author Philipe Lizotte
 * classe de vue qui s'occupe du menu, gere les entrées de l'utilisateur , gere les formes.
 * @version 0.1,
 */
public class VueMenu extends Observable{
    /**
     * comboBox permettant d'afficher les forme.
     * comboBox contien une liste de String.
     */
    private JComboBox<String> combobox;
    /**
     * ButtonGroup permet de grouper les boutons qui vont permetre
     * de choisir le type de contour.
     */
    private ButtonGroup groupRadio;
    /**
     * ContIntBouton est le bouton permettant de faire une forme ayant un contoure
     * et un interieur.
     * @version 0.1,
     */
    private JRadioButton contIntBouton;
    /**
     * intBouton est le bouton permettant de faire une forme ayant un interieur seulement.
     * @version 0.1,
     */

    private JRadioButton intBouton;
    /**
     * contBouton est le bouton permettant de faire une forme ayant un contoure seulement.
     * @version 0.1,
     */
    private JRadioButton contBouton;
    /**
     * jpanel, jpanel2 et jpanel3 vont etre les panels du menu ,
     * il contienne les comboBox ainsi que les boutons
     * @version 0.1,
     */
    private JPanel jpanel;
    private JPanel panel2;
    private JPanel panel3;
    /**
     *listeForme recevera une forme lorsquelle sera créer, il va alors stocker tout les formes créer.
     * @author Philipe Lizotte
     * @version 0.1,
     */
    private LinkedList<Forme> listeForme;
    /**
     * Vue menu créer les 3 panels ainsi que lance les ActionListener.
     * execute la fonction creerBouton et radioButtons;
     * @version 0.1,
     */
    public VueMenu() {
        GridLayout gridLayout = new GridLayout(1, 1);
        //créer le panel
        jpanel = new JPanel(gridLayout);
        panel2 = new JPanel(gridLayout);
        panel3 = new JPanel(gridLayout);
        creerbouton(jpanel);
        radioBoutons();
        combobox.addActionListener(new comboBoxActionListener());
        combobox.addActionListener(new comboTest());
        listeForme = new LinkedList<Forme>();

    }

    /**
     * getter de listForme
     * @return la list de forme
     */
    public LinkedList<Forme> getListeForme() {
        return listeForme;
    }

    /**
     * getJpanel renvoie jpanel
     * @return panel
     */
    public JPanel getJpanel() {
        return jpanel;
    }
    /**
     * getJpanel renvoie jpanel2
     * @return panel2
     */
    public JPanel getJpanel2() {
        return panel2;
    }
    /**
     * getJpanel renvoie jpanel3
     * @return panel3
     */
    public JPanel getJpanel3() {
        return panel3;
    }

    /**
     * Créerbouton permet de créer les boutons Dessiner, Effacer.
     * créer un ButtonGroup, ajoute les 2 boutons dans un buttonGroup.
     * ajoute ensuite les boutons a panel.
     * @param jpanel premier jpanel.
     */
    public void creerbouton(JPanel jpanel){
        //Créer les boutons et les radioboutton.
        JRadioButton dessinBouton = new JRadioButton("Dessiner");
        JRadioButton effacerBouton = new JRadioButton("Effacer");
        JButton ajouteBouton = new JButton("créer");
        //ajoute les boutons au panel
        jpanel.add(dessinBouton);
        jpanel.add(ajouteBouton);
        jpanel.add(effacerBouton);
        //créer le bouttongroup et insert les radio boutons dans le boutongroup
        groupRadio = new ButtonGroup();
        //créer le 2iem boutongroup et insert les radios boutons.
        ButtonGroup Radio2 = new ButtonGroup();
        Radio2.add(dessinBouton);
        Radio2.add(effacerBouton);
        jpanel.add(dessinBouton);
        jpanel.add(effacerBouton);

        //Combobox
        String forme[] = {"","carre", "ligne", "rectangle", "cercle",
                "elipse", "triangle"};
        combobox  = new JComboBox(forme);
        //ajoute combobox au panel
        jpanel.add(combobox);
    }
    /**
     * RadioBoutons créer les 3 Radio boutons permetant de choisire
     * le type d'affichage de la forme.
     * ajoute les 3 Radio boutons au groupe de boutons.
     * ajoute les 3 radion boutons au jpanel.
     */
    public void radioBoutons(){
        contBouton = new JRadioButton("contour seulement");
        //JRadioButton InterieurBouton = new JRadioButton("Interieur seulement");
        intBouton = new JRadioButton("interieur seulement");
        //JRadioButton contIntBouton = new JRadioButton("Contour et interieur");
        contIntBouton = new JRadioButton("contour et interieur");
        //ButtonGroup Radio = new ButtonGroup();
        groupRadio.add(contBouton);
        contBouton.addActionListener(new contOuInt());
        groupRadio.add(intBouton);
        intBouton.addActionListener(new contOuInt());
        groupRadio.add(contIntBouton);
        contIntBouton.addActionListener(new contIntBouton());
        jpanel.add(contBouton);
        jpanel.add(contIntBouton);
        jpanel.add(intBouton);

    }
    /**
     *vide le panel3 et affiche 2 choix de couleur si contIntboutons est selectionner.
     * @author Philipe Lizotte
     */
    public void choixCouleur(int buton){

        panel3.removeAll();
        panel3.revalidate();
        panel3.repaint();

        JColorChooser interieur = new JColorChooser();
        AbstractColorChooserPanel[] panels = interieur.getChooserPanels();
        for (AbstractColorChooserPanel accp : panels) {
            if(!accp.getDisplayName().equals("RGB")) {

            }
        }

        panel3.add(interieur);
        if(buton==1){
            JColorChooser contour = new JColorChooser();
            AbstractColorChooserPanel[] panel = contour.getChooserPanels();
            for (AbstractColorChooserPanel accp : panel) {
                if(!accp.getDisplayName().equals("RGB")) {

                }
            }
            panel3.add(contour);
        }
    }
    /**
     * ActionListener de comboBox
     * Créer tout les Jtextfield ,
     * vide le panel 2 et 3
     * vérifie la quelle forme est selectionner dans le comboBox
     *affiche seulement les coordonner necéssaire a la création de la forme.

     */
    private class comboBoxActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent aEvenement) {
            JTextField ligneX1 = new JTextField("X1",1);
            JTextField ligneX2 = new JTextField("X2",2);
            JTextField ligneX3 = new JTextField("X3",3);
            JTextField ligneX4 = new JTextField("X4",4);
            //attribut carre
            JTextField xGCcarre = new JTextField("X gauche");
            JTextField yGCarre = new JTextField("Y haut");
            JTextField largCarre = new JTextField("Largeur");
            //attribut rectangle
            JTextField hautRect = new JTextField("Hauteur");
            //attribut elipse
            JTextField centreX = new JTextField("centre X");
            JTextField centreY = new JTextField("centre Y");
            JTextField rayon = new JTextField("rayon");
            //attribut triangle
            JTextField triangley1 = new JTextField("Y");
            JTextField triangley2 = new JTextField("Y2");
            JTextField triangley3 = new JTextField("Y3");
            panel2.removeAll();
            panel2.revalidate();
            panel2.repaint();
            panel3.removeAll();
            panel3.revalidate();
            panel3.repaint();
            if(combobox.getSelectedItem()=="ligne"){
                informerObserver();
                panel2.add(ligneX1);
                panel2.add(ligneX2);
                panel2.add(ligneX3);
                panel2.add(ligneX4);
            }
            else if(combobox.getSelectedItem()=="carre"){
                panel2.add(xGCcarre);
                panel2.add(yGCarre);
                panel2.add(largCarre);
            }else if(combobox.getSelectedItem()=="rectangle"){
                panel2.add(xGCcarre);
                panel2.add(yGCarre);
                panel2.add(largCarre);
                panel2.add(hautRect);
            }else if(combobox.getSelectedItem()=="cercle"){
                panel2.add(centreX);
                panel2.add(centreY);
                panel2.add(rayon);
            }else if(combobox.getSelectedItem()=="elipse"){
                panel2.add(centreX);
                panel2.add(centreY);
                panel2.add(hautRect);
                panel2.add(largCarre);
            }else if(combobox.getSelectedItem()=="triangle"){
                panel2.add(ligneX1);
                panel2.add(ligneX2);
                panel2.add(ligneX3);
                panel2.add(triangley1);
                panel2.add(triangley2);
                panel2.add(triangley3);
            }

        }
    }
    /**
     * ActionListener de contIntBouton qui va afficher l'autre ColorChooser.
     */
    private class contIntBouton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent aEvenement) {
            int select;
            choixCouleur(select=1);

        }

    }
    /**
     * ActionListener de boutons contour et interieur.
     * s'assure qu'il n'y est qu'un choix de couleurs si
     * l'un de ces 2 boutons est selectionner
     */
    private class contOuInt implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent aEvenement) {
            int select;
            choixCouleur(select=0);

        }

    }
    /**
     * ActionListener de comboBox affiche les boutons permettant le choix d'affichage des forme.
     * si liste selectionner n'affiche pas les boutons.
     */
    private class comboTest implements ActionListener  {

        public void actionPerformed(ActionEvent e) {

            if(combobox.getSelectedItem()=="ligne"){
                contIntBouton.setVisible(false);
                contBouton.setVisible(false);
                intBouton.setVisible(false);
            }
            else{

                contIntBouton.setVisible(true);
                contBouton.setVisible(true);
                intBouton.setVisible(true);
            }
        }
    }
}
