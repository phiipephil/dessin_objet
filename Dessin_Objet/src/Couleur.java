import java.awt.Color;
/**
 * @author Philipe Lizotte
 * classe qui permet la création d'une couleur en utilisant RGB.
 * @version 0.1,
 */
public class Couleur extends Color {
    /**
     *Constructeur de Couleur.
     * @param r couleur rouge
     * @param g couleur verte
     * @param b couleur bleu
     */
    public Couleur(int r, int g, int b) {
        super(r, g, b);
    }


}
