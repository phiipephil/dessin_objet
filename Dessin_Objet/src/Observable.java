import java.util.LinkedList;

/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * définit les propriéter d'un objet 'observable'
 */
public class Observable {
    /**
     * la list d'observeur de l'observable
     */
    private LinkedList<Observer> listObserver;

    /**
     * constructeur de la classe observable
     * initialise la list d'observeur
     */
    public Observable(){
        listObserver = new LinkedList<Observer>();
    }

    /**
     * permet d'ajouter un observeur à l'observable
     * @param aobserver l'observeur à ajouter à l'observable
     */
    public void ajouterObserver(Observer aobserver){
        listObserver.add(aobserver);
    }

    /**
     * permet de supprimer un Observeur de la list des observeurs
     * de l'observable
     * @param aobserver l'observeur a supprimer
     */
    public  void suprimerObserver(Observer aobserver){
      listObserver.remove(aobserver);

    }

    /**
     * permet d'informer chauque observeur de l'observable
     */
    public void informerObserver(){
        for(Observer laObserver: listObserver){
            laObserver.ajouterForme(this);
        }
    }

}
