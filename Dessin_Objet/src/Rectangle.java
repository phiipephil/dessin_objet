

/**
 * @author  Philipe Lizotte
 * @version %I%, %G%
 * class du modele qui représente un rectangle elle
 * hérite de formeCouleurCentral
 */
public class Rectangle extends FormeCouleurCentral{
    /**
     * la coordonner en x du coter gauche du rectangle
     */
    private int XGauche;
    /**
     * la coordonner en y du haut du rectangle
     */
    private int yHaut;
    /**
     * la largeur du rectangle
     */
    private int largeur;
    /**
     * la hauteur du renctangle
     */
    private int hauteur;

    /**
     * Constructeur de la class rectagnle utilise le constructeur
     * de sa class parent
     * @param aCouleur la couleur du contour du rectangle
     * @param aCouleurCentral la couleur du centre du rectangle
     * @param XGauche la coordonner en x du coter gauche du rectangle
     * @param yHaut la coordonner en y du haut du rectangle
     * @param largeur la largeur du rectangle
     * @param hauteur la hauteur du rectangle
     * @param arempli un boolean affin de savoir si le rectangle serra rempli
     */
    public Rectangle(Couleur aCouleur, Couleur aCouleurCentral,
                     int XGauche, int yHaut, int largeur, int hauteur, boolean arempli) {
        super(aCouleur,aCouleurCentral,arempli);
        setXGauche(XGauche);
        setyHaut(yHaut);
        setLargeur(largeur);
        setHauteur(hauteur);
    }


    /**
     * getter de xGauche
     * @return le xGauche du rectangle
     */
    public int getXGauche() {
        return XGauche;
    }

    /**
     * setter de xGauche
     * @param XGauche la coordonner en x du coter gauche du
     * rectangle qui sera 'setter'
     */
    public void setXGauche(int XGauche) {
        this.XGauche = XGauche;
    }

    /**
     * gettr de yHaut
     * @return le yHaut du rectangle
     */
    public int getyHaut() {
        return yHaut;
    }

    /**
     * setter de yHaut
     * @param yHaut la coordonner en y du haut du rectangle
     */
    public void setyHaut(int yHaut) {
        this.yHaut = yHaut;
    }

    /**
     * getter de largeur
     * @return la largeur du rectangle
     */
    public int getLargeur() {
        return largeur;
    }

    /**
     * setter de largeur
     * @param largeur la largeur qui sera 'setter' pour
     *                le rectangle
     */
    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    /**
     * getter de hauteur
     * @return la hauteur du rectangle
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * setter de hauteur
     * @param hauteur la hauteur du rectangle qui sera 'setter'
     */
    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

}