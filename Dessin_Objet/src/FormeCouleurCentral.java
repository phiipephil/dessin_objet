/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * classe du modele qui est parent de toutes les formes
 * qui pecède une couleur centrale.
 * cet classe hérite de Forme
 */
public class FormeCouleurCentral extends Forme {
    /**
     * La couleur centrale de la forme
     */
    private Couleur couleurCentral;
    /**
     * indique si la forme sera rempli (couleur dans le centre de la forme)
     */
    private boolean rempli;

    /**
     * getter de rempli
     * @return true si la forme sera rempli
     */
    public boolean isRempli() {
        return rempli;
    }

    /**
     * setter de rempli
     * @param arempli un boolean qui sera 'setter' a rempli
     */
    public void setRemplie(boolean arempli) {
        this.rempli = arempli;
    }

    /**
     * Constructeur de FormeCouleurCentral utilise le constructeur de
     * sa classe parent
     * @param aCouleur la couleur du contour de la forme
     * @param aCouleurCentral la couleur du centre de la forme
     * @param aremplie un boolean affin de savoir si la forme serra rempli
     */
    public FormeCouleurCentral(Couleur aCouleur, Couleur aCouleurCentral, boolean aremplie){
        super(aCouleur);
        setRemplie(aremplie);
        setCouleurCentral(aCouleurCentral);
    }

    /**
     * getter de couleurCentral
     * @return la couleur centrale de la forme
     */
    public Couleur getCouleurCentral() {
        return couleurCentral;
    }

    /**
     * setter de couleurCentral
     * @param couleurCentral la couleur central que sera 'setter' pour la forme
     */
    public void setCouleurCentral(Couleur couleurCentral) {
        this.couleurCentral = couleurCentral;
    }
}
