

/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * classe du modele qui represente un cercle et
 * qui herite de formCouleurCentral
 */
public class Cercle extends FormeCouleurCentral{
    /**
     * le rayon du cercle
     */
    private int rayon;
    /**
     * la coordonner en X du centre du cercle
     */
    private int centreX;
    /**
     * la coordonner en Y du centre du cercle
     */
    private int centreY;

    /**
     * constructeur de la class cercle, utilise le
     * constructeur de sa classe parent
     * @param aCouleur la couleur du contour du cercle
     * @param aCouleurCentral la couleur du centre du cercle
     * @param rayon le rayon du cercle
     * @param centreX la coordonner en X du centre du cercle
     * @param centreY la coordonner en Y du centre du cercle
     * @param arempli un boolean affin de savoir si le carre serra rempli
     */
    public Cercle(Couleur aCouleur, Couleur aCouleurCentral, int rayon,
                  int centreX, int centreY, boolean arempli) {
        super(aCouleur,aCouleurCentral,arempli);
        setRayon(rayon);
        setCentreX(centreX);
        setCentreY(centreY);
    }

    /**
     * getter du rayon
     * @return le rayon du cercle
     */
    public int getRayon() {
        return rayon;
    }

    /**
     * setter du rayon du cercle
     * @param rayon le rayon qui sera 'sette' pour le cercle
     */
    public void setRayon(int rayon) {
        this.rayon = rayon;
    }

    /**
     * getter de centreX
     * @return la coordonner en X du centre du cercle
     */
    public int getCentreX() {
        return centreX;
    }

    /**
     * setter du centre en X
     * @param centreX la coordonner en X qui sera 'setter'
     */
    public void setCentreX(int centreX) {
        this.centreX = centreX;
    }

    /**
     * getter de centreY
     * @return la coordonner en Y du centre du cercle
     */
    public int getCentreY() {
        return centreY;
    }

    /**
     * setter du centre en Y
     * @param centreY la coordonner en Y qui sera 'setter'
     */
    public void setCentreY(int centreY) {
        this.centreY = centreY;
    }

}