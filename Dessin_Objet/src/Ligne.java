
/**
 * @author Philipe Lizotte
 * @version %I%, %G%
 * classe du modele qui represente une ligne
 * elle hérite de la class Forme
 */
public class Ligne extends Forme {
    /**
     * la coordonner en X du début de la ligne
     */
    private int pointX1;
    /**
     * la coordonner en X de la fin de la ligne
     */
    private int pointX2;
    /**
     * la coordonner en Y du début de la ligne
     */
    private int pointY1;
    /**
     * la coordonner en Y de la fin de la ligne
     */
    private int pointY2;

    /**
     * constructeur de Ligne utilise le
     * constructeur de sa class parent
     * @param aCouleur la couleur de la ligne
     * @param pointX1 la coordonner en X du début de la ligne
     * @param pointX2 la coordonner en X de la fin de la ligne
     * @param pointY1 la coordonner en Y du début de la ligne
     * @param pointY2 la coordonner en Y de la fin de la ligne
     */
    public Ligne(Couleur aCouleur,
                 int pointX1, int pointX2, int pointY1, int pointY2) {
        super(aCouleur);
        setPointX1(pointX1);
        setPointX2(pointX2);
        setPointY1(pointY1);
        setPointY2(pointY2);
    }

    /**
     * getter de pointX1
     * @return la coordonner en X du point de départ
     * de la ligne
     */
    public int getPointX1() {
        return pointX1;
    }

    /**
     * setter de pointX1
     * @param pointX1 la coordonner en X qui sera 'setter' pour le
     * point de depart de la ligne
     */
    public void setPointX1(int pointX1) {
        this.pointX1 = pointX1;
    }

    /**
     * getter de pointX2
     * @return la coordonner en X du point de fin de la ligne
     */
    public int getPointX2() {
        return pointX2;
    }

    /**
     * setter de pointX2
     * @param pointX2 la coordonner en X qui sera 'setter' pour le
     * pour le point de fin de la ligne
     */
    public void setPointX2(int pointX2) {
        this.pointX2 = pointX2;
    }
    /**
     * getter de pointY1
     * @return la coordonner en Y du point de départ
     * de la ligne
     */
    public int getPointY1() {
        return pointY1;
    }
    /**
     * setter de pointX1
     * @param pointY1 la coordonner en Y qui sera 'setter' pour le
     * point de depart de la ligne
     */
    public void setPointY1(int pointY1) {
        this.pointY1 = pointY1;
    }
    /**
     * getter de pointY2
     * @return la coordonner en Y du point de fin de la ligne
     */
    public int getPointY2() {
        return pointY2;
    }
    /**
     * setter de pointX2
     * @param pointY2 la coordonner en Y qui sera 'setter' pour le
     * pour le point de fin de la ligne
     */
    public void setPointY2(int pointY2) {
        this.pointY2 = pointY2;
    }
}
